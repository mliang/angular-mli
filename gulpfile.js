var gulp = require('gulp');
var del = require('del');
var usemin = require('gulp-usemin');
var rev = require('gulp-rev');


gulp.task('package', function() {

  gulp.src('app/assets/images/*.*')
    .pipe(gulp.dest('dist/assets/images'));

  gulp.src([
      'app/assets/iconfont/iconfont.eot',
      'app/assets/iconfont/iconfont.svg',
      'app/assets/iconfont/iconfont.ttf',
      'app/assets/iconfont/iconfont.woff',
      'bower_components/bootstrap/dist/fonts/*.*'
    ])
    .pipe(gulp.dest('dist/assets/css'));

  gulp.src([
      'bower_components/bootstrap/dist/fonts/*.*'
    ])
    .pipe(gulp.dest('dist/assets/fonts'));

  gulp.src('app/modules/*/*.html')
    .pipe(gulp.dest('dist/modules'));

  gulp.src('app/index.html')
    .pipe(usemin({
      css: [rev],
      js: [rev]
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('clean', function(cb) {
  del('dist', cb);
});


gulp.task('default', ['package']);