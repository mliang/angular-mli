(function() {
  'use strict';

  angular.module('mlApp.menu', [])
    .config(['$stateProvider', MenuConfig])
    .controller('MenuController', ['$state', '$rootScope', 'ConsumerService', MenuController]);

  function MenuConfig($stateProvider) {
    $stateProvider
      .state('menu', {
        url: '/menu',
        templateUrl: 'modules/menu/menu.html',
        controller: 'MenuController',
        controllerAs: 'vm'
      });
  }

  function MenuController($state, $rootScope, SessionsService) {
    var vm = this;

    vm.logout = SessionsService.logout;

    activate();

    //////////////////////

    function activate() {}


  }
})();