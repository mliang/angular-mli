#angular-mli

### 启动

**安装依赖**

1. gulp打包相关

```
npm install

# gulp
# del
# gulp-usemin
# gulp-rev
```

2. 项目依赖相关

```
bower install

# angular
# angular-cookies
# angular-ui-router
# lodash
# bootstrap
# jquery
```

3. 项目运行相关（server）

```
cd server
npm install

# server是在express基础上做的二次开发，相关依赖包都为express的依赖
```

**启动项目**

```
npm start


#服务运行端口口3000
```

**浏览器访问**

```
http://localhost:3000/index.html
```

![image](https://gitee.com/mliang/angular-mli/raw/master/app/assets/images/homepage.png)

### gulp

**运行命令**

```
gulp clean #清除dist文件夹

gulp  #打包
```

> gulp的配置文件：gulpfile.js

> 主要是依赖usemin这个插件简化打包配置


### server

**静态发布**

> 由于将bower_components这个依赖包放置在app同级，所以本地运行时需要同时对bower_components和app两个文件夹都做静态发布

> 注：由于将bower_components外置，所以生产环境下建议使用打包后的dist文件夹部署，可以在部署的jekiness中配置打包脚本，通过上传动作触发代码库打包

**接口模拟**




### 接口文档