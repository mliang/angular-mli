(function() {
  'use strict';


  angular.module('mlApp')
    .factory('ConsumerService', ['$state', '$q', 'CommonService', ConsumerService]);

  function ConsumerService($state, $q, CommonService) {

    var service = {
      getConsumerInfo: getConsumerInfo
    };

    return service;

    ////////////////////

    function getConsumerInfo() {
      var userInfo = JSON.parse(localStorage.getItem('userInfo'));
      if (userInfo) {
        return userInfo;
      } else {
        $state.go('login');
      }
    }

  }
})();